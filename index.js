const express = require('express');
const helmet = require('helmet');
const bodyParser = require('body-parser');

const app = express();

//add some HTTP headers for security
app.use(helmet());

// parse application/json
app.use(bodyParser.json());

app.get('/' , (req, res) =>{

    res.send('Hello World!');

});

app.listen(3000, ()=>{

    console.log('Example app listening on port 3000!');
});